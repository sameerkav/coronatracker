## Features

1. Includes interactive map for Countries and States/Provinces
2. Shows Total and Yesterday Statistics in a graphical way
3. Shows Historical Data for past 30, 60, 90 days
4. Compare two countries on the basis of Cases, Recovered and Deaths

## Available Scripts

After cloning app, run following command:

### `npm install`

In the project directory, you can run:

### `yarn start`
### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Data Sources

1. [disease.sh](https://corona.lmao.ninja/)
2. [John Hopkins University](https://coronavirus.jhu.edu/)

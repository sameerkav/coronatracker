import React, { Component } from "react"
import { Pie } from "react-chartjs-2"
import axios from "axios"

export default class WorldPieChart extends Component {

    state = {
        todayLabels: ["Cases Yesterday", "Recovered Yesterday", "Deaths Yesterday"],
        todayData: [{
            data: [],
            backgroundColor: ['#6ac1de', '#82ca9d', '#ea5252']
        }],
        totalLabels: ["Cases", "Recovered", "Deaths"],
        totalData: [{
            data: [],
            backgroundColor: ['#6ac1de', '#82ca9d', '#ea5252']
        }]
    }

    componentDidMount() {
        var totData = []
        var todData = []
        axios.get("https://corona.lmao.ninja/v2/historical/all")
            .then((res) => processData(res.data))

        const processData = (data) => {
            var historical = []
            for (var i in data) {
                historical.push(data[i])
            }

            var cases = Object.values(historical[0])
            var deaths = Object.values(historical[1])
            var recovered = Object.values(historical[2])

            todData.push((Math.abs(cases[1] - cases[0])))
            todData.push(Math.abs(recovered[1] - recovered[0]))
            todData.push(Math.abs(deaths[1] - deaths[0]))

            let todArray = [...this.state.todayData]
            todArray[0] = { ...todArray[0], data: todData }
            this.setState({ todayData: todArray })
        }

        totData.push(this.props.worldData.active)
        totData.push(this.props.worldData.recovered)
        totData.push(this.props.worldData.deaths)

        let totArray = [...this.state.totalData]
        totArray[0] = { ...totArray[0], data: totData }
        this.setState({ totalData: totArray })
    }

    render() {
        return (
            <div>
                <h3 style={{ paddingLeft: "25px", float: "left" }}>Data Distribution Total</h3>
                <Pie
                    data={{
                        labels: this.state.totalLabels,
                        datasets: this.state.totalData
                    }}
                    options={{
                        tooltips: {
                            callbacks: {
                                title: function (tooltipItem, data) {
                                    return data.labels[tooltipItem[0].index]
                                }
                            },
                            bodyFontSize: 16,
                        }
                    }}
                />
                <h3 style={{ paddingLeft: "25px", float: "left" }}>Data Distribution Yesterday</h3><br />

                <Pie
                    data={{
                        labels: this.state.todayLabels,
                        datasets: this.state.todayData
                    }}
                    options={{
                        tooltips: {
                            callbacks: {
                                title: function (tooltipItem, data) {
                                    return data.labels[tooltipItem[0].index]
                                }
                            },
                            bodyFontSize: 16
                        }
                    }}
                />

            </div>
        )
    }
}
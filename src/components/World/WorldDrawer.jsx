import React, { Component } from 'react'
import { SwipeableDrawer, Typography, AppBar, Tabs, Tab, Box } from "@material-ui/core"
import axios from "axios"

import WorldCards from "./WorldCards"
import WorldChart from "./WorldChart"
import CountryCompare from "./CountryCompare"

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

export default class WorldDrawer extends Component {

    state = {
        value: 0,
        worldData: []
    }

    componentDidMount() {
        axios.get("https://corona.lmao.ninja/v2/all?yesterday=false")
            .then((res) => this.setState({ worldData: res.data }))
    }

    togglePropsClose = () => {
        this.props.toggleDrawer(this.props.isSidebar, false)
    }

    togglePropsOpen = () => {
        this.props.toggleDrawer(this.props.isSidebar, true)
    }

    handleChange = (e, newValue) => {
        this.setState({ value: newValue })
    }


    render() {
        return (
            <div style={this.root}>
                <SwipeableDrawer
                    anchor="left"
                    open={this.props.isSidebar}
                    onClose={this.togglePropsClose}
                    onOpen={this.togglePropsOpen}
                >

                    <div style={{
                        width: "40vw", backgroundColor: "#f2f7ff"
                    }}>
                        <h1>< img style={{ width: "35px", height: "25px", margin: "0 20px" }} src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Flag_of_the_United_Nations.svg/1024px-Flag_of_the_United_Nations.svg.png" alt="" />
                            Earth
                        </h1>

                        <div>
                            <AppBar position="static">
                                <Tabs value={this.state.value} onChange={this.handleChange} aria-label="Simple tabs example" centered>
                                    <Tab label="Cards" {...a11yProps(0)} />
                                    <Tab label="Historic Data" {...a11yProps(1)} />
                                    <Tab label="Compare Countries" {...a11yProps(2)} />
                                </Tabs>
                            </AppBar>

                            <TabPanel value={this.state.value} index={0} >
                                <h3 style={{ paddingLeft: "25px" }}>Data Cards</h3>
                                <WorldCards worldData={this.state.worldData} />
                            </TabPanel>

                            <TabPanel value={this.state.value} index={1} >
                                <div style={{ height: "85vh", backgroundColor: "#f2f7ff" }}>
                                    <h3 style={{ paddingLeft: "25px" }}>Charts</h3>
                                    <WorldChart worldData={this.state.worldData} />
                                </div>
                            </TabPanel>

                            <TabPanel value={this.state.value} index={2} >
                                <div style={{ height: "85vh", backgroundColor: "#f2f7ff" }}>
                                    <CountryCompare />
                                </div>
                            </TabPanel>

                        </div>
                    </div>

                </SwipeableDrawer >
            </div >
        )
    }

    root = {
        flexGrow: 1
    }

}

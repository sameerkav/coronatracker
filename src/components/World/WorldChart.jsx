import React, { Component } from "react"
import { LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip, Legend } from "recharts"
import axios from "axios"
import { CircularProgress } from "@material-ui/core"

export default class WorldChart extends Component {

    state = {
        historicalWorld: null,
        isLoaded: false
    }

    componentDidMount() {

        axios.get("https://corona.lmao.ninja/v2/historical/all")
            .then((res) => processData(res.data))

        const processData = (data) => {
            var historical = []
            for (var i in data) {
                historical.push(data[i])
            }

            var dates = Object.keys(historical[0])
            var cases = Object.values(historical[0])
            var deaths = Object.values(historical[1])
            var recovered = Object.values(historical[2])

            function Data(date, cases, deaths, recovered) {
                this.date = date;
                this.cases = cases;
                this.deaths = deaths;
                this.recovered = recovered;
            }

            var processedData = [new Data(dates[0], cases[0], deaths[0], recovered[0])]
            for (var k = 1; k <= dates.length - 1; k++) {
                processedData.push(new Data(dates[k], Math.abs(cases[k] - cases[k - 1]), Math.abs(deaths[k] - deaths[k - 1]), Math.abs(recovered[k] - recovered[k - 1])))
            }

            this.setState({ historicalWorld: processedData.slice(1, 32) })
            this.setState({ isLoaded: true })
        }
    }

    render() {
        return (
            this.state.isLoaded ?
                <div>
                    <h4 style={{ paddingLeft: "25px" }}>Historic data for past 30 days</h4>
                    <LineChart
                        width={500} height={400}
                        data={this.state.historicalWorld}
                        margin={{ top: 20, right: 30, left: 20, bottom: 5 }}>
                        <XAxis dataKey="date" />
                        <CartesianGrid
                            stroke="#eee"
                            strokeDasharray="5 5"
                        />
                        <YAxis />
                        <Legend />
                        <Tooltip />
                        <Line type="monotone"
                            dataKey="cases"
                            stroke="#6ac1de" dot={false}
                            strokeWidth={3}
                        />
                        <Line
                            type="monotone"
                            dataKey="recovered"
                            stroke="#82ca9d" dot={false}
                            strokeWidth={3}
                        />
                        <Line
                            type="monotone"
                            dataKey="deaths"
                            stroke="#ea5252" dot={false}
                            strokeWidth={3}
                        />
                    </LineChart>
                </div>
                : <CircularProgress />
        )
    }
}

import React, { Component } from "react"
import { Card, CardContent, Typography, Toolbar } from "@material-ui/core"

import WorldPieChart from "./WorldPieChart"

export default class WorldCards extends Component {
    render() {
        return (
            <>
                <Toolbar>
                    <Card variant="outlined" style={this.cardStyle}>
                        <CardContent>
                            <Typography color="textSecondary" style={{ color: "#8884d8" }} ><b>
                                Current</b>
                            </Typography>
                            <hr />
                            <Typography>
                                {this.props.worldData.active.toLocaleString(undefined)}
                            </Typography>
                        </CardContent>
                    </Card>

                    <Card variant="outlined" style={this.cardStyle}>
                        <CardContent>
                            <Typography color="textSecondary" style={{ color: "#82ca9d" }}><b>
                                Recovered</b>
                            </Typography>
                            <hr />
                            <Typography>
                                {this.props.worldData.recovered.toLocaleString(undefined)}
                            </Typography>
                        </CardContent>
                    </Card>

                    <Card variant="outlined" style={this.cardStyle}>
                        <CardContent>
                            <Typography color="textSecondary" style={{ color: "#ea5252" }}><b>
                                Dead</b>
                            </Typography>
                            <hr />
                            <Typography>
                                {this.props.worldData.deaths.toLocaleString(undefined)}
                            </Typography>
                        </CardContent>
                    </Card>
                </Toolbar>

                <div>
                    <WorldPieChart worldData={this.props.worldData} />
                </div>

            </>
        )
    }

    cardStyle = {
        float: "left",
        margin: "15px 15px 30px 25px",
        width: "120px",
    }

}

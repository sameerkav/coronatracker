import React, { PureComponent } from 'react'
import axios from "axios"
import { Toolbar, FormControl, Select, MenuItem } from '@material-ui/core'
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, } from 'recharts';

export default class CountryCompare extends PureComponent {

    state = {
        countries: [],
        data: [],
        country1: "India",
        country2: "Italy",
        isLoaded: false
    }

    componentDidMount() {
        axios
            .get("https://disease.sh/v2/countries")
            // eslint-disable-next-line
            .then((res) => (processData(res.data), this.setState({ countries: res.data })));

        const processData = (data) => {

            var newData = []
            data.map(country1 => {
                if (country1.country === this.state.country1) {
                    data.map(country2 => {
                        if (country2.country === this.state.country2) {
                            var countryA = this.state.country1
                            var countryB = this.state.country2
                            newData.push({ name: "Cases", [countryA]: country1.cases, [countryB]: country2.cases })
                            newData.push({ name: "Recovered", [countryA]: country1.recovered, [countryB]: country2.recovered })
                            newData.push({ name: "Deaths", [countryA]: country1.deaths, [countryB]: country2.deaths })
                        }
                        return 0
                    })
                }
                return 0
            })
            this.setState({ data: newData, isLoaded: true })
        }
    }

    selectChangeCountry1 = (e) => {
        this.setState({ country1: e.target.value }, function () {
            var newData = []
            this.state.countries.map(country1 => {
                if (country1.country === this.state.country1) {
                    this.state.countries.map(country2 => {
                        if (country2.country === this.state.country2) {
                            var countryA = this.state.country1
                            var countryB = this.state.country2
                            newData.push({ name: "Cases", [countryA]: country1.cases, [countryB]: country2.cases })
                            newData.push({ name: "Recovered", [countryA]: country1.recovered, [countryB]: country2.recovered })
                            newData.push({ name: "Deaths", [countryA]: country1.deaths, [countryB]: country2.deaths })
                        }
                        return 0
                    })
                    this.setState({ data: newData, isLoaded: true })
                }
                return 0
            })
        })
    }

    selectChangeCountry2 = (e) => {
        this.setState({ country2: e.target.value }, function () {
            var newData = []
            this.state.countries.map(country2 => {
                if (country2.country === this.state.country2) {
                    this.state.countries.map(country1 => {
                        if (country1.country === this.state.country1) {
                            var countryA = this.state.country1
                            var countryB = this.state.country2
                            newData.push({ name: "Cases", [countryA]: country1.cases, [countryB]: country2.cases })
                            newData.push({ name: "Recovered", [countryA]: country1.recovered, [countryB]: country2.recovered })
                            newData.push({ name: "Deaths", [countryA]: country1.deaths, [countryB]: country2.deaths })
                        }
                        return 0
                    })
                    this.setState({ data: newData, isLoaded: true })
                }
                return 0
            })
        })
    }

    render() {
        return (
            this.state.isLoaded ?
                <div>
                    <Toolbar>
                        <h4 style={{ marginRight: "15px" }}>Country 1</h4>
                        <FormControl>
                            <Select
                                value={this.state.country1}
                                onChange={this.selectChangeCountry1}
                                style={{ marginRight: "2rem" }}
                            >
                                {this.state.countries.map(country => (
                                    <MenuItem value={country.country}>{country.country}</MenuItem>
                                ))}

                            </Select>
                        </FormControl>
                        <h4 style={{ marginRight: "15px" }}>Country 2</h4>
                        <FormControl>
                            <Select
                                value={this.state.country2}
                                onChange={this.selectChangeCountry2}
                                style={{ marginRight: "2rem" }}
                            >
                                {this.state.countries.map(country => (
                                    <MenuItem value={country.country}>{country.country}</MenuItem>
                                ))}

                            </Select>
                        </FormControl>
                    </Toolbar>

                    <BarChart
                        width={500}
                        height={300}
                        data={this.state.data}
                        margin={{
                            top: 30, right: 30, left: 20, bottom: 5,
                        }}
                    >
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="name" />
                        <YAxis />
                        <Tooltip />
                        <Legend />
                        <Bar dataKey={this.state.country1} fill="#6ac1de" />
                        <Bar dataKey={this.state.country2} fill="#ea5252" />
                    </BarChart>

                </div> : null
        )
    }
}

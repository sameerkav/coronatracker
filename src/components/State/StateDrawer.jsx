import React, { Component } from 'react'
import { SwipeableDrawer, Typography, AppBar, Tabs, Tab, Box } from "@material-ui/core"

import StateCards from "./StateCards"

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

export default class CountryDrawer extends Component {

    state = {
        value: 0,
    }

    togglePropsClose = () => {
        this.props.toggleDrawer(this.props.isSidebar, false)
    }

    togglePropsOpen = () => {
        this.props.toggleDrawer(this.props.isSidebar, true)
    }

    handleChange = (e, newValue) => {
        this.setState({ value: newValue })
    }


    render() {
        return (
            <div style={this.root}>
                <SwipeableDrawer
                    anchor="right"
                    open={this.props.isSidebar}
                    onClose={this.togglePropsClose}
                    onOpen={this.togglePropsOpen}
                >

                    <div style={{
                        width: "40vw", backgroundColor: "#f2f7ff"
                    }}>
                        <div style={{ marginLeft: "2.2rem" }} >
                            <h2>
                                {this.props.selectedState.province}
                            </h2>
                            <h3>
                                {this.props.selectedState.country}
                            </h3>
                        </div>

                        <div>
                            <AppBar position="static">
                                <Tabs value={this.state.value} onChange={this.handleChange} aria-label="Simple tabs example" centered>
                                    <Tab label="Cards" {...a11yProps(0)} />
                                </Tabs>
                            </AppBar>

                            <TabPanel value={this.state.value} index={0} >
                                <h3 style={{ marginRight: "2.2rem" }}>Data Cards</h3>
                                <StateCards selectedState={this.props.selectedState} />
                            </TabPanel>

                        </div>
                    </div>

                </SwipeableDrawer >
            </div >
        )
    }

    root = {
        flexGrow: 1
    }

}

import React, { Component } from "react";
import StateDrawer from "./StateDrawer"
import { Map, Marker, Popup, TileLayer } from "react-leaflet";
import { Icon } from 'leaflet'
import L from "leaflet";
import * as ELG from 'esri-leaflet-geocoder'
import { Button } from "@material-ui/core";

const corona = new Icon({
    iconUrl: "https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-yellow.png",
    iconSize: [12, 16]
})

var southWest = L.latLng(-89.98155760646617, -180)
var northEast = L.latLng(89.99346179538875, 180);
var bounds = L.latLngBounds(southWest, northEast);

export default class StateMapComponent extends Component {

    state = {
        selectedState: null,
        isSidebar: false
    }

    componentDidMount() {
        const map = this.leafletMap.leafletElement;
        new ELG.Geosearch().addTo(map);
        setTimeout(map.invalidateSize.bind(map));
        map.setMaxBounds(bounds);
        map.on('drag', function () {
            map.panInsideBounds(bounds, { animate: false });
        });
    };

    toggleDrawer = (anchor, open) => (event) => {
        this.setState({ isSidebar: open });
    }

    render() {
        return (
            <Map center={[34, 74]} zoom={3} ref={m => {
                this.leafletMap = m;
            }} minZoom={3} >
                <TileLayer
                    url="https://{s}.tile.jawg.io/jawg-sunny/{z}/{x}/{y}{r}.png?access-token={accessToken}"
                    attribution='<a href="http://jawg.io" title="Tiles Courtesy of Jawg Maps" target="_blank">&copy; <b>Jawg</b>Maps</a> &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                    accessToken="6AC281bAgEzzbmuK8Cryu0kmxetSuMsWJvzwAs6hesxy5zRTMDY1p7XYlqZNYJlM"
                />
                {this.props.states.filter(wstate => wstate.province != null).map(wstate => (
                    <Marker position={[
                        wstate.coordinates.latitude,
                        wstate.coordinates.longitude
                    ]}
                        onclick={() => {
                            this.setState({ selectedState: wstate })
                        }}
                        icon={corona}
                    />
                ))}

                {this.state.selectedState && (
                    <>
                        <Popup
                            position={[this.state.selectedState.coordinates.latitude, this.state.selectedState.coordinates.longitude]}
                            onClose={() => {
                                this.setState({ selectedState: null })
                            }}
                        >
                            <div>
                                <h2><b> {this.state.selectedState.province ? this.state.selectedState.province : this.state.selectedState.country} </b></h2>
                                <hr />
                                <p>Confirmed: {this.state.selectedState.stats.confirmed.toLocaleString(undefined)}</p>
                                <p>
                                    Recovered: {this.state.selectedState.stats.recovered.toLocaleString(undefined)}
                                </p>
                                <p>Deaths: {this.state.selectedState.stats.deaths.toLocaleString(undefined)}</p>
                                <hr />

                                <Button color='primary' onClick={this.toggleDrawer(this.state.isSidebar, true)}>
                                    Details
                                </Button>

                            </div>
                        </Popup>

                        <StateDrawer
                            isSidebar={this.state.isSidebar}
                            toggleDrawer={this.toggleDrawer()}
                            selectedState={this.state.selectedState} />
                    </>
                )}
            </Map>
        );
    }
}

import React, { Component } from "react"
import { Pie } from "react-chartjs-2"

export default class CountryPieChart extends Component {

    state = {
        totalLabels: ["Confirmed", "Recovered", "Deaths"],
        totalData: [{
            data: [],
            backgroundColor: ['#6ac1de', '#82ca9d', '#ea5252']
        }]
    }

    componentDidMount() {
        var totData = []

        totData.push(this.props.selectedState.stats.confirmed)
        totData.push(this.props.selectedState.stats.recovered)
        totData.push(this.props.selectedState.stats.deaths)

        let totArray = [...this.state.totalData]
        totArray[0] = { ...totArray[0], data: totData }
        this.setState({ totalData: totArray })
    }

    render() {
        return (
            <div>
                <h3 style={{ marginRight: "2.2rem", float: "left" }}>Data Distribution Total</h3>
                <Pie
                    data={{
                        labels: this.state.totalLabels,
                        datasets: this.state.totalData
                    }}
                    options={{
                        tooltips: {
                            callbacks: {
                                title: function (tooltipItem, data) {
                                    return data.labels[tooltipItem[0].index]
                                }
                            },
                            bodyFontSize: 16,
                        }
                    }}
                />
            </div>
        )
    }
}
import React, { Component } from "react";
import { AppBar, Toolbar, Typography, IconButton } from "@material-ui/core";
import PublicIcon from '@material-ui/icons/Public';
import { Icon } from '@iconify/react';
import virusOutline from '@iconify/icons-mdi/virus-outline';
import { Select, MenuItem, FormControl } from "@material-ui/core";

import WorldDrawer from "./World/WorldDrawer"

export default class Header extends Component {

    state = {
        isSidebar: false
    }

    toggleDrawer = (anchor, open) => (event) => {
        this.setState({ isSidebar: open });
    }

    render() {
        return (
            <>
                <AppBar position="static">
                    <Toolbar>
                        <Icon icon={virusOutline} width="25" height="25" />
                        <Typography variant="h6" style={{ flexGrow: 1 }}>
                            Corona Tracker
                        </Typography>
                        <h4>World Data</h4>
                        <IconButton aria-label="World" component="span" style={{ marginRight: "1.5rem" }} onClick={this.toggleDrawer(this.state.isSidebar, true)}>
                            <PublicIcon style={{ color: "white" }} />
                        </IconButton>
                        <Typography style={{ marginRight: "1rem" }}>Mode</Typography>
                        <FormControl>
                            <Select
                                value={this.props.isCountry ? "Country" : "State"}
                                onChange={this.props.selectChange}
                                style={{ color: "white", marginRight: "2rem" }}
                            >
                                <MenuItem value={"Country"}>Country</MenuItem>
                                <MenuItem value={"State"}>State</MenuItem>
                            </Select>
                        </FormControl>
                    </Toolbar>
                </AppBar>

                <WorldDrawer
                    isSidebar={this.state.isSidebar}
                    toggleDrawer={this.toggleDrawer()}
                />

            </>
        );
    }
}   
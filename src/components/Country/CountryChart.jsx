import React, { PureComponent } from "react"
import { LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip, Legend } from "recharts"
import axios from "axios"
import { CircularProgress, FormControl, Select, MenuItem, Toolbar } from "@material-ui/core"

export default class CountryChart extends PureComponent {

    state = {
        useData: null,
        historicalCountry: null,
        count: 60,
        isLoaded: false
    }

    componentDidMount() {

        axios.get("https://corona.lmao.ninja/v2/historical/" + this.props.selectedCountry.country + "?lastdays=90")
            .then((res) => processData(res.data.timeline))

        const processData = (data) => {
            var historical = []
            for (var i in data) {
                historical.push(data[i])
            }

            var dates = Object.keys(historical[0])
            var cases = Object.values(historical[0])
            var deaths = Object.values(historical[1])
            var recovered = Object.values(historical[2])

            function Data(date, cases, deaths, recovered) {
                this.date = date;
                this.cases = cases;
                this.deaths = deaths;
                this.recovered = recovered;
            }

            var processedData = [new Data(dates[0], cases[0], deaths[0], recovered[0])]
            for (var k = 1; k <= dates.length - 1; k++) {
                processedData.push(new Data(dates[k], Math.abs(cases[k] - cases[k - 1]), Math.abs(deaths[k] - deaths[k - 1]), Math.abs(recovered[k] - recovered[k - 1])))
            }

            this.setState({ historicalCountry: processedData })
            this.setState({ useData: this.state.historicalCountry.slice(91 - this.state.count, 91) })
            this.setState({ isLoaded: true })
        }
    }

    selectChange = (e) => {
        this.setState({ count: e.target.value }, function () {
            this.setState({ useData: this.state.historicalCountry.slice(91 - this.state.count, 91) })
        })
    }

    render() {
        return (
            this.state.isLoaded ?
                <div>
                    <h4 style={{ marginRight: "2.2rem" }}>Historic data for past {this.state.count} days</h4>

                    <Toolbar>
                        <h4 style={{ marginRight: "15px" }}>Number of days</h4>
                        <FormControl>
                            <Select
                                value={this.state.count}
                                onChange={this.selectChange}
                                style={{ marginRight: "2rem" }}
                            >
                                <MenuItem value={30}>30</MenuItem>
                                <MenuItem value={60}>60</MenuItem>
                                <MenuItem value={90}>90</MenuItem>
                            </Select>
                        </FormControl>
                    </Toolbar>


                    <LineChart
                        width={500} height={400}
                        data={this.state.useData}
                        margin={{ top: 20, right: 30, left: 20, bottom: 5 }}>
                        <XAxis dataKey="date" />
                        <CartesianGrid
                            stroke="#eee"
                            strokeDasharray="5 5"
                        />
                        <YAxis />
                        <Legend />
                        <Tooltip />
                        <Line type="monotone"
                            dataKey="cases"
                            stroke="#6ac1de" dot={false}
                            strokeWidth={3}
                        />
                        <Line
                            type="monotone"
                            dataKey="recovered"
                            stroke="#82ca9d" dot={false}
                            strokeWidth={3}
                        />
                        <Line
                            type="monotone"
                            dataKey="deaths"
                            stroke="#ea5252" dot={false}
                            strokeWidth={3}
                        />
                    </LineChart>
                </div>
                : <CircularProgress />
        )
    }
}

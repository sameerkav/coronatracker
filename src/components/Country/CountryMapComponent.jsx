import React, { Component } from "react";
import { Button } from "@material-ui/core";
import { Map, Marker, Popup, TileLayer } from "react-leaflet";
import L from "leaflet";
import { Icon } from "leaflet"
import * as ELG from "esri-leaflet-geocoder"

import CountryDrawer from "./CountryDrawer"

const corona = new Icon({
    iconUrl: "https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png",
    iconSize: [13, 20]
})

var southWest = L.latLng(-89.98155760646617, -180)
var northEast = L.latLng(89.99346179538875, 180);
var bounds = L.latLngBounds(southWest, northEast);

export default class CountryMapComponent extends Component {
    state = {
        selectedCountry: null,
        isSidebar: false
    }

    componentDidMount() {
        const map = this.leafletMap.leafletElement;
        new ELG.Geosearch().addTo(map);
        setTimeout(map.invalidateSize.bind(map));
        map.setMaxBounds(bounds);
        map.on('drag', function () {
            map.panInsideBounds(bounds, { animate: false });
        });

    };

    toggleDrawer = (anchor, open) => (event) => {
        this.setState({ isSidebar: open });
    }

    render() {
        return (
            <Map center={[34, 74]} zoom={3} ref={m => {
                this.leafletMap = m;
            }} minZoom={3} >
                <TileLayer
                    url="https://{s}.tile.jawg.io/jawg-sunny/{z}/{x}/{y}{r}.png?access-token={accessToken}"
                    attribution='<a href="http://jawg.io" title="Tiles Courtesy of Jawg Maps" target="_blank">&copy; <b>Jawg</b>Maps</a> &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                    accessToken={process.env.REACT_APP_MAP_API}
                    noWrap
                />

                {this.props.countries.map(country => (
                    <Marker position={[
                        country.countryInfo.lat,
                        country.countryInfo.long
                    ]}
                        onclick={() => {
                            this.setState({ selectedCountry: country })
                        }}
                        icon={corona}
                    />
                ))}

                {this.state.selectedCountry && (
                    <>
                        <Popup
                            position={[this.state.selectedCountry.countryInfo.lat, this.state.selectedCountry.countryInfo.long]}
                            onClose={() => {
                                this.setState({ selectedCountry: null })
                            }}
                        >
                            <div>
                                <h2><b>{this.state.selectedCountry.country} </b></h2>
                                <hr />
                                <p>Confirmed: {this.state.selectedCountry.cases.toLocaleString(undefined)}</p>
                                <p>Recovered: {this.state.selectedCountry.recovered.toLocaleString(undefined)}</p>
                                <p>Deaths: {this.state.selectedCountry.deaths.toLocaleString(undefined)}</p>
                                <hr />
                                <Button
                                    color='primary'
                                    onClick={this.toggleDrawer(this.state.isSidebar, true)}
                                >
                                    Details
                                </Button>


                            </div>
                        </Popup>

                        <CountryDrawer
                            isSidebar={this.state.isSidebar}
                            toggleDrawer={this.toggleDrawer()}
                            selectedCountry={this.state.selectedCountry}
                        />

                    </>
                )}

            </Map>
        );
    }
}

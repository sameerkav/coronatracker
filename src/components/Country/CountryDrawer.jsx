import React, { Component } from 'react'
import { SwipeableDrawer, Typography, AppBar, Tabs, Tab, Box } from "@material-ui/core"

import CountryCards from "./CountryCards"
import CountryChart from "./CountryChart"

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

export default class CountryDrawer extends Component {

    state = {
        value: 0,
    }

    togglePropsClose = () => {
        this.props.toggleDrawer(this.props.isSidebar, false)
    }

    togglePropsOpen = () => {
        this.props.toggleDrawer(this.props.isSidebar, true)
    }

    handleChange = (e, newValue) => {
        this.setState({ value: newValue })
    }


    render() {
        return (
            <div style={this.root}>
                <SwipeableDrawer
                    anchor="right"
                    open={this.props.isSidebar}
                    onClose={this.togglePropsClose}
                    onOpen={this.togglePropsOpen}
                >

                    <div style={{
                        width: "40vw", backgroundColor: "#f2f7ff"
                    }}>
                        <h1>< img style={{ width: "35px", height: "25px", margin: "0 20px" }} src={this.props.selectedCountry.countryInfo.flag} alt="" />
                            {this.props.selectedCountry.country}
                        </h1>

                        <div>
                            <AppBar position="static">
                                <Tabs value={this.state.value} onChange={this.handleChange} aria-label="Simple tabs example" centered>
                                    <Tab label="Cards" {...a11yProps(0)} />
                                    <Tab label="Historic Data" {...a11yProps(1)} />
                                </Tabs>
                            </AppBar>

                            <TabPanel value={this.state.value} index={0} >
                                <h3 style={{ marginRight: "2.2rem" }}>Data Cards</h3>
                                <CountryCards selectedCountry={this.props.selectedCountry} />
                            </TabPanel>

                            <TabPanel value={this.state.value} index={1} >
                                <div style={{ height: "85vh", backgroundColor: "#f2f7ff" }}>
                                    <h3 style={{ marginRight: "2.2rem" }}>Charts</h3>
                                    <CountryChart selectedCountry={this.props.selectedCountry} />
                                </div>
                            </TabPanel>

                        </div>
                    </div>

                </SwipeableDrawer >
            </div >
        )
    }

    root = {
        flexGrow: 1
    }

}

import React, { Component } from "react"
import { Card, CardContent, Typography, Toolbar } from "@material-ui/core"

import CountryPieChart from "./CountryPieChart"

export default class CountryCards extends Component {
    render() {
        return (
            <>
                <Toolbar>
                    <Card variant="outlined" style={this.cardStyle}>
                        <CardContent>
                            <Typography color="textSecondary" style={{ color: "#8884d8" }} ><b>
                                Current</b>
                            </Typography>
                            <hr />
                            <Typography>
                                {this.props.selectedCountry.active.toLocaleString(undefined)}
                            </Typography>
                        </CardContent>
                    </Card>

                    <Card variant="outlined" style={this.cardStyle}>
                        <CardContent>
                            <Typography color="textSecondary" style={{ color: "#82ca9d" }}><b>
                                Recovered</b>
                            </Typography>
                            <hr />
                            <Typography>
                                {this.props.selectedCountry.recovered.toLocaleString(undefined)}
                            </Typography>
                        </CardContent>
                    </Card>

                    <Card variant="outlined" style={this.cardStyle}>
                        <CardContent>
                            <Typography color="textSecondary" style={{ color: "#ea5252" }}><b>
                                Dead</b>
                            </Typography>
                            <hr />
                            <Typography>
                                {this.props.selectedCountry.deaths.toLocaleString(undefined)}
                            </Typography>
                        </CardContent>
                    </Card>
                </Toolbar>

                <div>
                    <CountryPieChart selectedCountry={this.props.selectedCountry} />
                </div>

            </>
        )
    }

    cardStyle = {
        float: "left",
        marginBottom: "1rem",
        marginRight: "2.2rem",
        marginTop: "1rem",
        width: "8rem",
    }

}

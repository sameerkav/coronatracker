import React, { Component } from "react";
import "./App.css";
import Header from "./components/Header";
import axios from "axios";
import { CircularProgress, Toolbar } from "@material-ui/core"
import "leaflet/dist/leaflet.css";
import "esri-leaflet-geocoder/dist/esri-leaflet-geocoder.css";
import "leaflet/dist/leaflet.js";
import "esri-leaflet-geocoder/dist/esri-leaflet-geocoder.js";

import CountryMapComponent from "./components/Country/CountryMapComponent";
import StateMapComponent from "./components/State/StateMapComponent"

class App extends Component {
    state = {
        countries: [],
        states: [],
        isLoading: true,
        isCountry: true
    };

    componentDidMount() {
        axios
            .get("https://disease.sh/v2/countries")
            .then((res) => this.setState({ countries: res.data }));
        axios
            .get("https://corona.lmao.ninja/v2/jhucsse")
            .then((res) => this.setState({ states: res.data, isLoading: false }));
    }

    selectChange = () => {
        this.setState({ isCountry: !this.state.isCountry })
    }

    render() {
        if (!this.state.isLoading) {
            return (
                <div className="App">
                    <Header isCountry={this.state.isCountry} selectChange={this.selectChange} />
                    {this.state.isCountry ? <CountryMapComponent countries={this.state.countries} />
                        : <StateMapComponent states={this.state.states} />
                    }
                </div>
            );
        } else {
            return (
                <div style={{ marginLeft: "40vw", marginTop: "50vh" }}>
                    <Toolbar>
                        <CircularProgress />
                        <h4>If page doesn't load, please refresh</h4>
                    </Toolbar>
                </div>
            )
        }
    }
}

export default App;
